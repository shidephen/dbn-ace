
from dbn.tensorflow.models import SupervisedDBNClassification
import librosa as lr
import numpy as np
from sklearn.preprocessing import normalize
from sklearn.decomposition import PCA

class DBNFeatureExtractor(SupervisedDBNClassification):
    def __init__(self, **kwargs):
        super(DBNFeatureExtractor, self).__init__(**kwargs)
        from dbn.tensorflow.models import sess
        self.sess = sess

    def get_feature(self, X):
        if len(X.shape) == 1:  # It is a single sample
            X = np.expand_dims(X, 0)
        
        feed_dict = { self.visible_units_placeholder: X }
        feed_dict.update({placeholder: 1.0 for placeholder in self.keep_prob_placeholders})
        features = self.sess.run(
            self.transform_op,
            feed_dict=feed_dict)

        return features


_FS = 8000
_FFT_SIZE = 4096
_HOP_LEN = int(0.064 * _FS)
_WIN_SIZE = int(0.5 * _FS)
_CUT_SIZE = 1400
_PCA_LEN = int(_CUT_SIZE * 0.3)


def calc_signal_feature(music_file):
    """
    Calculate spectrum feature from music_file
    Args:
        music_file:
    Returns:
        narray[n_frames, n_features]
    """
    x, _ = lr.load(music_file, _FS)
    X = lr.stft(x, _FFT_SIZE, _HOP_LEN, _WIN_SIZE, 'blackman')
    # Calculate spectrum
    mag, _ = lr.magphase(X)
    cutted_mag = mag[:1400, :]
    # Transpose for scikit learn
    cutted_mag = np.transpose(cutted_mag)
    # L2 normalization
    normaled_mag = normalize(cutted_mag)
    # Dimensionality reduction
    pca = PCA(n_components=_PCA_LEN, whiten=True)
    features = pca.fit_transform(normaled_mag)

    return features


def calc_temporal_features(dbn_features):
    """
    Caculate temporal features from dbn features
    Args:
        dbn_features: narray [n_frames, n_features]
    """
    