import tensorflow as tf


class TFModel(object):
    def __init__(self, input_size=200, rnn_layers=[100], output_size=96,
                 batch_size=32, max_seq_len=600, learning_rate=0.003, dropout_keep=0.5):
        self.input_size = input_size
        self.learning_rate = learning_rate
        self.max_seq_len = max_seq_len
        self.batch_size = batch_size
        self.output_size = output_size
        self.rnn_layers = rnn_layers

        if len(rnn_layers) == 1:
            self.cell = tf.contrib.rnn.BasicLSTMCell(rnn_layers[0])
        else:
            cells = []
            for layer in rnn_layers:
                cell = tf.contrib.rnn.BasicLSTMCell(layer)
                cell = tf.contrib.rnn.DropoutWrapper(cell)
                cells.append(cell)
            self.cell = tf.contrib.rnn.MultiRNNCell(cells)
            self.cell()

        # Variables
        self.features_holder = tf.placeholder(tf.float32, [None, max_seq_len, input_size])
        self.output_holder = tf.placeholder(tf.float32, [None, max_seq_len])

    def _build_model(self):
        # Unroll RNN
        with tf.variable_scope('tf_scope', reuse=True):
            self.Wzh = [tf.get_variable('Wzh{}'.format(i), [l, self.output_size], tf.float32)
                        for i, l in enumerate(self.rnn_layers)]
            self.bias_zh = [tf.get_variable('bzh{}'.format(i), [l], tf.float32)
                            for i, l in enumerate(self.rnn_layers)]
            self.Whz = [tf.get_variable('Whz{}'.format(i), [self.output_size, l], tf.float32)
                        for i, l in enumerate(self.rnn_layers)]
            self.bias_hz = [tf.get_variable('bhz{}'.format(i), [self.output_size], tf.float32)
                            for i, l in enumerate(self.rnn_layers)]


        def loop_fn_initial():
            next_state = self.cell.zero_state(self.batch_size, tf.float32)
            next_input = tf.zeros([self.batch_size, self.input_size])
            return next_input, next_state

        def loop_fn_trans(time, cell_output, cell_state):
            pass

        def loop_fun(time, cell_output, cell_state, loop_state):
            pass

        pass

    def _preprocess(self, batches):
        pass

    
    def load_model(model_path):
        pass

    def save_model(model_path):
        pass

    def train(X, Y):
        """
        X: shape(batch_size, seq_len, feature_size)
        Y: shape(batch_size, seq_len, labels)
        """
        pass

    def eval(X):
        pass
